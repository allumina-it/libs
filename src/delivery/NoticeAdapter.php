<?php
/**
 * VGallery: CMS based on FormsFramework
 * Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @package VGallery
 *  @subpackage core
 *  @author Alessandro Stucchi <wolfgan@gmail.com>
 *  @copyright Copyright (c) 2004, Alessandro Stucchi
 *  @license http://opensource.org/licenses/gpl-3.0.html
 *  @link https://github.com/wolfgan43/vgallery
 */
namespace phpformsframework\libs\delivery;

use phpformsframework\libs\App;
use phpformsframework\libs\Error;

abstract class NoticeAdapter extends App {
    protected $recipients                                   = array();
    protected $connection                                   = null;
    protected $actions                                      = array();

    public function __construct($connection = null)
    {
        $this->connection                                   = $connection;
    }

    public abstract function checkRecipient($target);
    public abstract function send($message);
    public abstract function sendLongMessage($title, $fields = null, $template = null);

    protected abstract function process();

    public function addAction($name, $url) {
        $this->actions[$url]                                = $name;
    }
    public function addRecipient($target, $name = null) {
        if($this->checkRecipient($target)) {
            $this->recipients[$target]                      = ($name ? $name : $target);
        }
    }
    protected function getResult() {
        return (Error::check("notice")
            ? Error::raise("notice")
            : false
        );
    }
}